var express = require('express');
var server = express();
var logger = require('morgan');
server.set('json spaces', 2);

server.use(logger('dev'));
module.exports = server;
