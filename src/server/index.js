module.exports = {
  create: function () {
    var server = require('./server')
    return server
  },
  defaults: require('./defaults'),
  router: require('./router'),
  rewriter: require('./rewriter')
}
