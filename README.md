# API Server
## Install

```bash
$ npm install -g api-server
```


## Example

Create a `db.json` file

```json
{
  "posts": [
    { "id": 1, "title": "api-server", "author": "mario" }
  ],
  "comments": [
    { "id": 1, "body": "some comment", "postId": 1 }
  ],
  "profile": { "name": "mario" }
}
```

Start API Server

```bash
$ api-server --watch db.json
```

Now if you go to [http://localhost:3000/posts/1](), you'll get

```json
{ "id": 1, "title": "api-server", "author": "mario" }
```

Also when doing requests, its good to know that
- If you make POST, PUT, PATCH or DELETE requests, changes will be automatically and safely saved to `db.json`.
- Your request body JSON should be object enclosed, just like the GET output. (for example `{"name": "Foobar"}`)
- Id values are not mutable. Any `id` value in the body of your PUT or PATCH request wil be ignored. Only a value set in a POST request wil be respected, but only if not already taken.
- A POST, PUT or PATCH request should include a `Content-Type: application/json` header to use the JSON in the request body. Otherwise it wil result in a 200 OK but without changes being made to the data.

## Routes

Based on the previous `db.json` file, here are all the default routes. You can also add [other routes](#add-routes) using `--routes`.

### Plural routes

```
GET    /posts
GET    /posts/1
POST   /posts
PUT    /posts/1
PATCH  /posts/1
DELETE /posts/1
```

### Singular routes

```
GET    /profile
POST   /profile
PUT    /profile
PATCH  /profile
```

### Filter

Use `.` to access deep properties

```
GET /posts?title=api-server&author=typicode
GET /posts?id=1&id=2
GET /comments?author.name=typicode
```

### Slice

Add `_start` and `_end` or `_limit` (an `X-Total-Count` header is included in the response)

```
GET /posts?_start=20&_end=30
GET /posts/1/comments?_start=20&_end=30
GET /posts/1/comments?_start=20&_limit=10
```

### Sort

Add `_sort` and `_order` (ascending order by default)

```
GET /posts?_sort=views&_order=DESC
GET /posts/1/comments?_sort=votes&_order=ASC
```

### Operators

Add `_gte` or `_lte` for getting a range

```
GET /posts?views_gte=10&views_lte=20
```

Add `_ne` to exclude a value

```
GET /posts?id_ne=1
```

Add `_like` to filter (RegExp supported)

```
GET /posts?title_like=server
```

### Full-text search

Add `q`

```
GET /posts?q=internet
```

### Relationships

To include children resources, add `_embed`

```
GET /posts?_embed=comments
GET /posts/1?_embed=comments
```

To include parent resource, add `_expand`

```
GET /comments?_expand=post
GET /comments/1?_expand=post
```

To get or create nested resources (by default one level, [add routes](#add-routes) for more)

```
GET  /posts/1/comments
POST /posts/1/comments
```

### Database

```
GET /db
```

### Homepage

Returns default index file or serves `./public` directory

```
GET /
```


### Alternative port

You can start JSON Server on other ports with the `--port` flag:

```bash
$ api-server --watch db.json --port 3004
```

### Access from anywhere

You can access your fake API from anywhere using CORS and JSONP.

### Add routes

Create a `routes.json` file. Pay attention to start every route with /.

```json
{
  "/api/": "/",
  "/blog/:resource/:id/show": "/:resource/:id"
}
```

Start JSON Server with `--routes` option.

```bash
api-server db.json --routes routes.json
```

Now you can access resources using additional routes.

```bash
/api/posts
/api/posts/1
/blog/posts/1/show
```

### CLI usage

```
api-server [options] <source>

Options:
  --watch, -w        Watch file(s)                                    [boolean]
  --routes, -r       Path to routes file


You can also set options in a `api-server.json` configuration file.

```json
{
  "port": 3000
}
```

### Module

If you need to add authentication, validation, or __any behavior__, you can use the project as a module in combination with other Express middlewares.

__Simple example__

```js
// server.js
var apiServer = require('api-server')
var server = apiServer.create()
var router = apiServer.router('db.json')
var middlewares = apiServer.defaults()

server.use(middlewares)
server.use(router)
server.listen(3000, function () {
  console.log('JSON Server is running')
})
```

```sh
$ node server.js
```

For an in-memory database, you can pass an object to `apiServer.router()`.
Please note also that `apiServer.router()` can be used in existing Express projects.

__Custom routes example__

Let's say you want a route that echoes query parameters and another one that set a timestamp on every resource created.

```js
var apiServer = require('api`-server')
var server = apiServer.create()
var router = apiServer.router('db.json')
var middlewares = apiServer.defaults()

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares)

// Add custom routes before JSON Server router
server.get('/echo', function (req, res) {
  res.jsonp(req.query)
})

server.use(function (req, res, next) {
  if (req.method === 'POST') {
    req.body.createdAt = Date.now()
  }
  // Continue to JSON Server router
  next()
})

// Use default router
server.use(router)
server.listen(3000, function () {
  console.log('JSON Server is running')
})
```

__Custom output example__

To modify responses, overwrite `router.render` method:

```javascript
// In this example, returned resources will be wrapped in a body property
router.render = function (req, res) {
  res.jsonp({
   body: res.locals.data
  })
}
```

__Rewriter example__

To add rewrite rules, use `apiServer.rewriter()`:

```javascript
// Add this before server.use(router)
server.use(apiServer.rewriter({
  '/api/': '/',
  '/blog/:resource/:id/show': '/:resource/:id'
}))
```
